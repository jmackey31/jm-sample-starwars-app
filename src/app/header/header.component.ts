import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    mainNavActive = false;

    constructor() {
    }

    ngOnInit(): void {
    }

    onToggleMainNav = () => {
        this.mainNavActive = this.mainNavActive !== true;
    }

    onLogOutUser = () => {
        localStorage.removeItem('jm-starwars-user-logged-in');
        location.reload(true);
    }

}
