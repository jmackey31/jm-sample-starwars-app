import {Component, OnInit} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm = this.fb.group({
        username: ['', Validators.minLength(8)],
        password: ['', Validators.minLength(8)]
    });
    userLoggedIn = false;

    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        this.onCheckUserLoggedIn();
    }

    onCheckUserLoggedIn = () => {
        const storageCheck = localStorage.getItem('jm-starwars-user-logged-in');
        const checkUserLoggedIn = storageCheck && storageCheck === 'true';

        if (checkUserLoggedIn) {
            this.userLoggedIn = true;
        }
    };

    onLoginUser = () => {
        localStorage.setItem('jm-starwars-user-logged-in', 'true');
        location.reload(true);
    };

}
